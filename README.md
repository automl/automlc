![Java 8](https://img.shields.io/badge/java-8-blue.svg) 
![License](https://img.shields.io/badge/license-GPLv3-blue.svg) 

# AutoMLC
Automated Multi-Label Classification

## **Overview**

This project presents GA-Auto-MLC and Auto-MEKA_GGP, two methods that automatically select and configure multi-label classification algorithms based on the [MEKA tool](http://waikato.github.io/meka/). If you use this project in your work, please cite the following papers:

1. **GA-Auto-MLC**

  - A. G. C. de Sá, G. L. Pappa, and A. A. Freitas. Towards a method for automatically selecting and configuring multi-label classification algorithms. In Proceedings of the Genetic and Evolutionary Computation Conference Companion , pp. 1125–1132, 2017. [ [PDF](https://www.cs.kent.ac.uk/people/staff/aaf/pub_papers.dir/GECCO-2017-ECADA-Wksp-de-Sa.pdf) ] [ [ACM](https://dl.acm.org/citation.cfm?id=3082053) ]

2. **Auto-MEKA_GGP**

  - A. G. C. de Sá, G. L. Pappa, and A. A. Freitas. Automated Selection and Configuration of Multi-Label Classification Algorithms with Grammar-based Genetic Programming. In Proceedings of the  International Conference on Parallel Problem Solving from Nature (PPSN), 2018.  [ [PDF](https://www.cs.kent.ac.uk/people/staff/aaf/pub_papers.dir/PPSN-2018-de-Sa.pdf) ] [ [Springer](https://link.springer.com/chapter/10.1007/978-3-319-99259-4_25) ]
  
  - Note that Auto-MEKA_GGP uses modified versions of EpochX, MEKA and WEKA to perform automated multi-label classification. The versions of these frameworks can be found  at: [EpochX](https://github.com/alexgcsa/EpochX), [MEKA](https://github.com/alexgcsa/MEKA), and [WEKA](https://github.com/alexgcsa/WEKA)
  
  

## **Documentation**

The description of the multi-label classification (MLC) search is available in the following link: [MLC Search Space](https://github.com/laic-ufmg/automlc/blob/master/PPSN/MLC-SearchSpace.pdf)

We are still working on a tutorial to turn Auto-MEKA_GGP and GA-Auto-MLC easy methods to use.


## **Datasets**

Several datatasets are available at: [PPSN Datasets](https://github.com/laic-ufmg/automlc/tree/master/PPSN/datasets).

## **License**

See [LICENSE](https://github.com/laic-ufmg/automlc/blob/master/LICENSE) file.


## **Support**

Any questions or comments should be directed to Alex de Sá (alexgcsa@dcc.ufmg.br). You can also create an issue on the repository.



